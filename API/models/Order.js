const mongoose = require('mongoose')
const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required : [true, 'Total Amount is is required']
	},

	purchasedOn: {
		type: Date,
		default : new Date()
	},

	user: [
	{
		userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' , required : true},

		userEmail : {
			type: String,
			required : [true, 'Email is required']
		}
	}],

	cartItems : [
        {
            product: {
                type: String ,
                required : true 
            },
            name: {
                type: String ,
                 required : true 
             },
            quantity: {
                type: Number ,
                 default : 1 
             },
            price: {
                type: Number ,
                 required: true
             },
            subtotal: {
                type: Number ,
                 required: true
             }
        }
    ]
	
	
})

module.exports = mongoose.model('Order', orderSchema)
